## missi_phone_eea-user 13 TKQ1.221114.001 V14.0.2.0.TMSEUXM release-keys
- Manufacturer: xiaomi
- Platform: lahaina
- Codename: redwood
- Brand: Redmi
- Flavor: missi_phone_eea-user
- Release Version: 13
- Kernel Version: 5.4.210
- Id: TKQ1.221114.001
- Incremental: V14.0.2.0.TMSEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Redmi/redwood/redwood:13/RKQ1.211001.001/V14.0.2.0.TMSEUXM:user/release-keys
- OTA version: 
- Branch: missi_phone_eea-user-13-TKQ1.221114.001-V14.0.2.0.TMSEUXM-release-keys
- Repo: redmi_redwood_dump
